FROM ruby:2.6.5

RUN apt-get update && apt install -y curl
RUN curl -sL https://deb.nodesource.com/setup_12.x | bash -
RUN apt install -y nodejs

WORKDIR /tmp
COPY Gemfile* ./
RUN gem install bundler -v '2.1.4'
RUN bundle install

WORKDIR /alitwitter
COPY . ./

RUN npm install -g yarn && yarn

EXPOSE 3000

CMD ["rails", "server", "-b", "0.0.0.0"]