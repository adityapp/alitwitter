# Ali Twitter

Twitter-like web app used Rails-Framemwork which can do:

1. Create a tweet
2. Delete a tweet
3. List tweets


## Ruby version

The required Ruby version is **2.6.5**


## Database migration

```
rails db:migrate
```


## How to run the test suite

```
rails test
```

## How to run

```
rails server
```

using browser and put `http://localhost:3000/tweets`


## How to build Docker Image Artefact
You should have installed `Docker` in your machine

To build Docker Image
```
docker build -t <IMAGE_NAME> .
```

To run Docker Image in container
```
docker container run -p 3000:3000 <IMAGE_NAME>
```

Then you can access from browser with `localhost:3000`
